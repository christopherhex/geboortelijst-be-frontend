import got from "got";
import { parse } from "node-html-parser";
import { ErrorCodes } from "./errors";

const GB_BASE_URL_API = "https://wishlist.geboortelijst.be/wishlistapi.php";
const GB_BASE_URL = "https://wishlist.geboortelijst.be/wishlist.php";

// Strings that indicate that the user is not logged in anymore
const FAIL_STRINGS = [
  "Sorry, login niet gelukt.",
  "moet je eerst even (opnieuw) inloggen.",
];

/**
 * Validates the return HTML to check if the user is still logged in
 * @param {String} body
 */
function validateLogin(body) {
  for (const str of FAIL_STRINGS) {
    if (body.indexOf(str) >= 0) {
      throw ErrorCodes.AUTH_FAIL;
    }
  }
}

/**
 * Generate a random session id
 * @private
 * @param {Number} length
 * @returns {String} random SID string of specified length
 */
function generateSID(length = 28) {
  const CHARS =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let res = "";

  for (let i = 0; i < length; i++) {
    res += CHARS.charAt(Math.floor(Math.random() * CHARS.length));
  }

  return res;
}

/**
 * Required to setup a new session to geboortelijst.be
 * Links the SID to a certain webshop for subsequent sessions
 * @param {String} wsa
 */
export async function setupSession(wsa) {
  const sid = generateSID();
  // @TODO Response checking and error handling
  await got(`${GB_BASE_URL_API}?wsa=${wsa}&SID=${sid}`);
  return sid;
}

/**
 * Login to geboortelijst.be using username / password
 * Returns a SID session ID on successful login
 *
 * @param {String} wsa - The identification of the linked webshop
 * @param {String} username - Username of client
 * @param {String} password - Password
 */
export async function login(wsa, username, password) {
  let sid = await setupSession(wsa);

  console.log(`${wsa} - ${username} - ${password}`);

  // @TODO Response checking and error handling
  const res = await got.post(GB_BASE_URL_API, {
    form: {
      SID: sid,
      login: username,
      pwd: password,
    },
  });

  validateLogin(res.body);
  return sid;
}

/**
 * Get the list of favourites
 * @param {*} sid
 */
export async function getFavouriteList(sid) {
  const resultList = [];

  const res = await got(`${GB_BASE_URL}?SID=${sid}`);

  validateLogin(res.body);

  const rootHTML = parse(res.body);

  const listItems = rootHTML.querySelectorAll("tr");

  listItems.forEach((listItem) => {
    let resData = {};
    const cells = listItem.querySelectorAll("td");

    cells.forEach((cell, index) => {
      if (index == 0) {
        const aChild = cell.firstChild;
        const imgChild = aChild.firstChild;
        resData.url = aChild.getAttribute("href");
        resData.imageUrl = imgChild.getAttribute("src");
      } else if (index == 1) {
        // Extract
        const xIndex = cell.text.indexOf("x");
        resData.qty = parseInt(cell.text.slice(0, xIndex).trim());
        resData.description = cell.text.slice(xIndex + 1).trim();
      } else if (index == 2) {
        const price = cell.innerText
          .replace("&euro;", "")
          .replace("+", "")
          .replace("-", "")
          .replace(",", ".")
          .trim();
        resData.price = parseFloat(price);
      }
    });

    resultList.push(resData);
  });

  return resultList;
}
