export const getErrorMessage = (code) => code.message;

export const handleHTTPError = (code, res) => {
  if (code === ErrorCodes.AUTH_FAIL) {
    return res.status(401).send("Authentication Fail");
  } else {
    return res.status(500).send("Server Error");
  }
};

export const ErrorCodes = {
  AUTH_FAIL: "AUTH_FAIL",
};
