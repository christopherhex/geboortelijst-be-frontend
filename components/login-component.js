export default function LoginComponent() {
  const onLogin = async (event) => {
    event.preventDefault();

    const res = await fetch("/api/login", {
      body: JSON.stringify({
        username: event.target.username.value,
        password: event.target.password.value,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    });
  };

  return (
    <form
      onSubmit={onLogin}
      class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col"
    >
      <div class="mb-4">
        <label
          class="block text-grey-darker text-sm font-bold mb-2"
          htmlFor="username"
        >
          Gebruikersnaam
        </label>
        <input
          class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker"
          id="username"
          type="text"
          placeholder="Username"
        ></input>
      </div>
      <div class="mb-6">
        <label
          class="block text-grey-darker text-sm font-bold mb-2"
          for="password"
        >
          Wachtwoord
        </label>
        <input
          class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3"
          id="password"
          type="password"
          placeholder="******************"
        ></input>
      </div>
      <div class="flex items-center justify-between">
        <button class="bg-blue-500 rounded p-2 text-white" type="submit">
          Inloggen
        </button>
      </div>
    </form>
  );
}
