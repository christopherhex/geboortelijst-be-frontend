import Head from "next/head";
import Link from "next/link";

function MainPageButton({ text, link }) {
  return (
    <div className="bg-blue-400 m-1">
      <Link href={link}>
        <a>{text}</a>
      </Link>
    </div>
  );
}

export default function Home() {
  return (
    <div className="container mx-auto">
      <Head>
        <title>Geboortelijst</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="flex flex-col items-center justify-center h-screen">
          <h1 className="text-5xl">Geboortelijst</h1>
          <MainPageButton text="Favorieten" link="/list"></MainPageButton>
          <MainPageButton text="Login" link="/login"></MainPageButton>
        </div>
      </main>
    </div>
  );
}
