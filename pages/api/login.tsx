import { login } from '../../services/geboortelijst_be';
import { handleHTTPError } from '../../services/errors';
import Cookies from 'cookies';


const wsa = '266039'


export default async function handler(req, res) {

  if(req.method !== 'POST'){
    return res.send(400);
  }
  
  // https://maxschmitt.me/posts/next-js-cookies/
  const cookies = new Cookies(req, res);

  const { username, password } = req.body;

  try {
    const sid = await login(wsa, username, password);
    cookies.set('sid', sid);
    res.send(200);
  } catch (e) {
    return handleHTTPError(e.message, res);
  }

}