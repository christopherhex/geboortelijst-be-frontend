  import { getFavouriteList } from '../../services/geboortelijst_be';
  import { handleHTTPError } from '../../services/errors';
  import Cookies from 'cookies';

  export default async function handler(req, res) {

    // https://maxschmitt.me/posts/next-js-cookies/
    const cookies = new Cookies(req, res);

    const SID = cookies.get('sid');

    try {
      const data = await getFavouriteList(SID);

      const summary = data.reduce((acc, item) => {
        return {
          totalPrice: acc.totalPrice + Math.round(item.price),
          totalItems: acc.totalItems + item.qty
        }
  
      },{
        totalPrice: 0,
        totalItems: 0
      })
      
      res.status(200).json(
        {
          data,
          summary: {
            uniqueItems: data.length,
            ...summary
          }
        })
    } catch(e){
      return handleHTTPError(e.message, res);
    }

  }