import useSWR from 'swr'

const getListData = async () => {
  const response = await fetch('/api/data');
  return await response.json();
}

function List({ items }) {


  const sortedItems = items.sort((a, b) => a.price - b.price);
  const listItems = sortedItems.map((item, index) => {
    return (
      <div key={index} className="flex flex-col rounded-xl border-2 border-gray-100 m-2 justify-between">
        <div className="mx-auto text-xl text-center p-2" >{item.description}</div>
        <img className="w-40 h-40 rounded-xl p-1 mx-auto" src={item.imageUrl}></img>

        <div className="grid grid-cols-3 bg-gray-100">
          <div className="mx-auto"><span className="text-gray-400"># </span>{item.qty}</div>
          <div className="mx-auto font-bold">€ {item.price}</div>
          <div className="mx-auto"> <a href={item.url}>Link</a></div>
        </div>
      </div>
    )
  })
  
  return <div className="grid grid-cols-1 md:grid-cols-4 gap-4">{listItems}</div>
}


function ListHeader({ summary }) {
  return (
    <div className="flex flex-row justify-between bg-gray-200 m-2">
      <div>Totaal bedrag: € {summary.totalPrice}</div>
      <div>Aantal artikels: {summary.totalItems}</div>
      <div>Unieke artikels: {summary.uniqueItems}</div>


    </div>
  )
}


export default function Overview() {

  const { data, error } = useSWR('/api/data', getListData);

  if(!data) return <h1> loading... </h1>


  return (
    <div className="container mx-auto font-sans flex flex-col">
      <ListHeader summary={data.summary}></ListHeader>
      <List items={data.data}></List>
    </div>
  )




}
